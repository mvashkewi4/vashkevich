package com.example.vashkevich.fragments

import android.os.Bundle
import android.preference.PreferenceManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.example.vashkevich.MainActivity.Companion.DEFAULT_LAYOUT_KEY
import com.example.vashkevich.R
import kotlinx.android.synthetic.main.fragment_layout_select.*

/**
 * A simple [Fragment] subclass.
 */
class LayoutSelectFragment : Fragment() {
    private val onDefaultClickListener = View.OnClickListener {
        PreferenceManager.getDefaultSharedPreferences(activity?.applicationContext).edit()
                .putBoolean(DEFAULT_LAYOUT_KEY, true)
                .apply()
        radio_dense_layout.isChecked = false
        radio_default_layout.isChecked = true
    }
    private val onDenseClickListener = View.OnClickListener {
        PreferenceManager.getDefaultSharedPreferences(activity?.applicationContext).edit()
                .putBoolean(DEFAULT_LAYOUT_KEY, false)
                .apply()
        radio_default_layout.isChecked = false
        radio_dense_layout.isChecked = true
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_layout_select, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        radio_default_layout.setOnClickListener(onDefaultClickListener)
        radio_dense_layout.setOnClickListener(onDenseClickListener)
        layout_default_layout.setOnClickListener(onDefaultClickListener)
        layout_dense_layout.setOnClickListener(onDenseClickListener)
        if (PreferenceManager.getDefaultSharedPreferences(context).getBoolean(DEFAULT_LAYOUT_KEY, true)) {
            radio_default_layout.callOnClick()
        } else {
            radio_dense_layout.callOnClick()
        }
    }
}
