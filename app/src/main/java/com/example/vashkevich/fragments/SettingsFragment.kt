package com.example.vashkevich.fragments

import android.os.Bundle
import androidx.appcompat.app.AppCompatDelegate
import androidx.preference.Preference
import androidx.preference.PreferenceFragmentCompat
import com.example.vashkevich.MainActivity
import com.example.vashkevich.R

class SettingsFragment : PreferenceFragmentCompat() {
    override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {
        setPreferencesFromResource(R.xml.settings, rootKey)
    }

    override fun onPreferenceTreeClick(preference: Preference?): Boolean {
        if (preference?.key == MainActivity.LIGHT_THEME_KEY) {
            AppCompatDelegate.setDefaultNightMode(if (preference.sharedPreferences.getBoolean(MainActivity.LIGHT_THEME_KEY, true)) {
                AppCompatDelegate.MODE_NIGHT_NO
            } else {
                AppCompatDelegate.MODE_NIGHT_YES
            })
        }
        return super.onPreferenceTreeClick(preference)
    }
}