package com.example.vashkevich.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.vashkevich.AppHelper
import com.example.vashkevich.AppInfo
import com.example.vashkevich.R

class ListFragment : Fragment() {
    protected lateinit var mRecyclerView: RecyclerView
    protected lateinit var mAppList: List<AppInfo>
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        mAppList = AppHelper.getData(activity)

        mRecyclerView = view.findViewById(R.id.fragment_list_recycler_view)
        mRecyclerView.apply {
            adapter = AppAdapter(mAppList)
            layoutManager = LinearLayoutManager(activity)
        }
    }

    inner class AppHolder(view: View) : RecyclerView.ViewHolder(view), View.OnClickListener {

        private lateinit var mAppInfo: AppInfo
        private var mAppIconImageView: ImageView = view.findViewById(R.id.list_item_image_view)
        private var mAppNameTextView: TextView = view.findViewById(R.id.list_item_name_text_view)
        private var mPackageTextView: TextView = view.findViewById(R.id.list_item_package_text_view)

        init {
            view.setOnClickListener(this)
        }

        fun bindApp(appInfo: AppInfo) {
            mAppInfo = appInfo
            mAppNameTextView.text = appInfo.Name
            mPackageTextView.text = appInfo.Package
            mAppIconImageView.background = appInfo.Icon
        }

        override fun onClick(v: View?) {
            val packageName = mAppInfo.Package.toString()
            val intent = activity?.packageManager?.getLaunchIntentForPackage(packageName)
            startActivity(intent)
        }
    }

    inner class AppAdapter(apps: List<AppInfo>) : RecyclerView.Adapter<AppHolder>() {
        private var mApps: List<AppInfo> = apps

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AppHolder {
            val view = layoutInflater.inflate(R.layout.list_item, parent, false)
            return AppHolder(view)
        }

        override fun onBindViewHolder(holder: AppHolder, position: Int) {
            val appInfo = mApps[position]
            holder.bindApp(appInfo)
        }

        override fun getItemCount(): Int = mApps.size
    }
}
