package com.example.vashkevich.fragments

import android.os.Bundle
import android.preference.PreferenceManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatDelegate
import androidx.fragment.app.Fragment
import com.example.vashkevich.MainActivity.Companion.LIGHT_THEME_KEY
import com.example.vashkevich.R
import kotlinx.android.synthetic.main.fragment_theme_select.*

/**
 * A simple [Fragment] subclass.
 */
class ThemeSelectFragment : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_theme_select, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        radio_light.setOnClickListener {
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO)
            PreferenceManager.getDefaultSharedPreferences(activity?.applicationContext).edit()
                    .putBoolean(LIGHT_THEME_KEY, true)
                    .apply()
            radio_light.isChecked = true
            radio_dark.isChecked = false
        }
        radio_dark.setOnClickListener {
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES)
            PreferenceManager.getDefaultSharedPreferences(activity?.applicationContext).edit()
                    .putBoolean(LIGHT_THEME_KEY, false)
                    .apply()
            radio_light.isChecked = false
            radio_dark.isChecked = true
        }
        if (PreferenceManager.getDefaultSharedPreferences(activity?.applicationContext).getBoolean(LIGHT_THEME_KEY, true)) {
            radio_light.callOnClick()
        } else {
            radio_dark.callOnClick()
        }
    }
}