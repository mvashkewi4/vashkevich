package com.example.vashkevich

import android.app.Activity
import android.content.Intent
import android.util.Log

class AppHelper {
    companion object {
        fun getData(activity: Activity?): MutableList<AppInfo> {
            val intent = Intent(Intent.ACTION_MAIN).apply {
                addCategory(Intent.CATEGORY_LAUNCHER)
                removeCategory(Intent.CATEGORY_HOME)
            }
            val pm = activity?.packageManager
            val activities = pm?.queryIntentActivities(intent, 0) ?: emptyList()
            Log.i("ACTIVITIES", "Found " + activities.size)
            val result = mutableListOf<AppInfo>()
            activities.forEach {
                if (it.activityInfo.packageName != activity?.packageName) {
                    result.add(AppInfo(
                            it.loadLabel(pm),
                            it.activityInfo.packageName,
                            it.loadIcon(pm)
                    ))
                }
            }
            return result
        }
    }
}
