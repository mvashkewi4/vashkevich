package com.example.vashkevich

import android.content.Intent
import android.os.Bundle
import android.preference.PreferenceManager
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.app.AppCompatDelegate
import androidx.appcompat.widget.Toolbar
import androidx.drawerlayout.widget.DrawerLayout
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.navigateUp
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import com.google.android.material.navigation.NavigationView
import com.yandex.metrica.YandexMetrica

import com.yandex.metrica.YandexMetricaConfig





class MainActivity : AppCompatActivity() {

    companion object {
        const val FIRST_LAUNCH_KEY = "FIRST_LAUNCH"
        const val LIGHT_THEME_KEY = "THEME"
        const val DEFAULT_LAYOUT_KEY = "LAYOUT"
    }

    private lateinit var appBarConfiguration: AppBarConfiguration
    private val API_key = "5ab075f1-f9cc-40b5-86fc-448a00bbcf17"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        // Creating an extended library configuration.

        // Creating an extended library configuration.
        val config = YandexMetricaConfig.newConfigBuilder(API_key).build()
        // Initializing the AppMetrica SDK.
        YandexMetrica.activate(applicationContext, config)
        // Automatic tracking of user activity.
        YandexMetrica.enableActivityAutoTracking(application)

        val applicationPreferences = PreferenceManager.getDefaultSharedPreferences(applicationContext)
        if (applicationPreferences.getBoolean(FIRST_LAUNCH_KEY, true)) {
            applicationPreferences.edit()
                    .putBoolean(FIRST_LAUNCH_KEY, false)
                    .putBoolean(LIGHT_THEME_KEY, true)
                    .putBoolean(DEFAULT_LAYOUT_KEY, true)
                    .apply()
            val intent = Intent(this, WelcomeActivity::class.java)
            startActivity(intent)
            finishAffinity()
        }
        if (PreferenceManager.getDefaultSharedPreferences(applicationContext).getBoolean(LIGHT_THEME_KEY, true)) {
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO)
        } else {
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES)
        }
        setContentView(R.layout.activity_main)
        val toolbar: Toolbar = findViewById(R.id.toolbar)
        setSupportActionBar(toolbar)

        val drawerLayout: DrawerLayout = findViewById(R.id.drawer_layout)
        val navView: NavigationView = findViewById(R.id.nav_view)
        val navController = findNavController(R.id.nav_host_fragment)
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        appBarConfiguration = AppBarConfiguration(setOf(
                R.id.nav_grid, R.id.nav_list), drawerLayout)
        setupActionBarWithNavController(navController, appBarConfiguration)
        navView.setupWithNavController(navController)
        navView.getHeaderView(0).findViewById<View>(R.id.navImageView).setOnClickListener {
            val intent = Intent(this, ProfileActivity::class.java)
            startActivity(intent)
        }
    }

    override fun onSupportNavigateUp(): Boolean {
        val navController = findNavController(R.id.nav_host_fragment)
        return navController.navigateUp(appBarConfiguration) || super.onSupportNavigateUp()
    }

    override fun onBackPressed() {
        val navController = findNavController(R.id.nav_host_fragment)
        if (navController.currentDestination?.id != navController.graph.startDestination) {
            super.onBackPressed()
        }
    }


}
