package com.example.vashkevich

import android.graphics.drawable.Drawable

data class AppInfo(
        var Name: CharSequence,
        var Package: CharSequence,
        var Icon: Drawable
)