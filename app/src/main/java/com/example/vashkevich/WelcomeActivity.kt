package com.example.vashkevich

import android.content.Intent
import android.os.Bundle
import android.preference.PreferenceManager
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.app.AppCompatDelegate
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentStatePagerAdapter
import com.example.vashkevich.MainActivity.Companion.LIGHT_THEME_KEY
import com.example.vashkevich.fragments.DescriptionFragment
import com.example.vashkevich.fragments.LayoutSelectFragment
import com.example.vashkevich.fragments.ThemeSelectFragment
import com.example.vashkevich.fragments.WelcomeFragment
import kotlinx.android.synthetic.main.activity_welcome.*


class WelcomeActivity : AppCompatActivity() {

    private var fragments = listOf<Fragment>(WelcomeFragment(), DescriptionFragment(), ThemeSelectFragment(), LayoutSelectFragment())

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        if (PreferenceManager.getDefaultSharedPreferences(applicationContext).getBoolean(LIGHT_THEME_KEY, true)) {
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO)
        } else {
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES)
        }
        setContentView(R.layout.activity_welcome)

        @Suppress("DEPRECATION")
        welcome_view_pager.adapter = object : FragmentStatePagerAdapter(supportFragmentManager) {
            override fun getItem(position: Int): Fragment {
                return fragments[position]
            }

            override fun getCount(): Int {
                return fragments.size
            }
        }
        next_button.setOnClickListener {
            if (welcome_view_pager.currentItem == 3) {
                val intent = Intent(this, MainActivity::class.java)
                startActivity(intent)
                finishAffinity()
            } else {
                welcome_view_pager.arrowScroll(View.FOCUS_RIGHT)
            }
        }
    }

    override fun onBackPressed() {
        if (welcome_view_pager.currentItem == 0) {
            super.onBackPressed()
        } else {
            welcome_view_pager.arrowScroll(View.FOCUS_LEFT)
        }
    }
}
